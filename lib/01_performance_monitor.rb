def measure(n = 1)
    time = Time.now
    n.times do yield end 
    (Time.now - time)/n

end
