def reverser()
  array = yield.split(' ')
  i = 0
  while i < array.length
    array[i] = array[i].reverse
    i = i + 1
  end
  array.join(' ')
end

def adder(default = 1)
  yield + default
end

def repeater(number = 1)
  number.times do yield end
end
